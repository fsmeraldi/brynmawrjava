

Sample java notebooks from [CS206 Data Structures](https://jupyter.brynmawr.edu/services/public/dblank/CS206%20Data%20Structures/2016-Spring/Notebooks/) taught by Prof. [Douglas Blank](https://cs.brynmawr.edu/~dblank/) at Bryn Mawr.

The Dockerfile and requirements.txt are copied from the sample iJava binder at https://mybinder.org/v2/gh/SpencerPark/ijava-binder/master

Find the binder here: [![Binder](https://mybinder.org/badge_logo.svg)](https://mybinder.org/v2/git/https%3A%2F%2Fbitbucket.org%2Ffsmeraldi%2Fbrynmawrjava%2Fsrc%2Fmaster/master)
